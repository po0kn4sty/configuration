set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

Plugin 'tpope/vim-fugitive'

Plugin 'vim-ruby/vim-ruby'

Plugin 'tpope/vim-rails'

Plugin 'pangloss/vim-javascript'

Plugin 'molokai'

Plugin 'solarized'

Plugin 'Valloric/YouCompleteMe'

Plugin 'chriskempson/base16-vim'

Plugin 'chriskempson/vim-tomorrow-theme'

Plugin 'derekwyatt/vim-scala'
call vundle#end()            " required

filetype plugin indent on
syntax enable
set background=dark
set t_Co=256
colorscheme Tomorrow-Night-Eighties
set autoindent smartindent 
set tabstop=2 shiftwidth=2 softtabstop=2 expandtab
set nu 
set nobackup
set ignorecase noerrorbells novisualbell t_vb= tm=500 
set incsearch lazyredraw magic hlsearch
set backspace=eol,start,indent whichwrap+=<,>,h,l autoread 
set wildmenu wildignore=*.o,*~,*.pyc
set mouse=a
set showmatch smarttab

" Always show the status line
set laststatus=2
" Format the status line
set statusline=\ %{HasPaste()}%F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l\ "Column:\ %c

"" Helper Functions 
" Delete trailing white space on save, useful for Python and CoffeeScript ;)
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()

" Returns true if paste mode is enabled
function! HasPaste()
  if &paste
    return 'PASTE MODE  '
  en
  return ''
endfunction

